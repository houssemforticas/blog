<?php

class DbConnect
{
    // TODO changer la variable $cnx (statique)
    public $cnx;

    public function __construct()
    {

        $db_setting = parse_ini_file(__DIR__.'/../conf/conf_data.ini');

        try {
            $this->cnx = new PDO(
                "mysql:host={$db_setting['host']};dbname={$db_setting['dbname']};port={$db_setting['port']};charset=UTF8",
                $db_setting['username'],
                $db_setting['password']);

        } catch (PDOException $e) {
            echo $e->getMessage();
            die;
        }
    }

    public function getOne($className, $args)
    {
        $classNameLower = strtolower($className);
        $sql = "SELECT * From {$classNameLower} WHERE ";

        foreach (array_keys($args) as $key => $value)
        {
            $sql .= $value.' = :'.$value;
            if($key < count($args)- 1)
            {
                $sql .=' AND ';
            }
        }

        $stmt = $this->cnx->prepare($sql);

        foreach ($args as $key => $value)
        {
            $stmt->bindParam(':'.$key, $value);

        }

        $stmt->execute();
        $stmt->setFetchMode(PDO::FETCH_CLASS, $className);
        $result = $stmt->fetch();

        return $result;
    }
    public function getMany($className, $args = [])
    {
        $classNameLower = strtolower($className);
        $sql = "SELECT * From {$classNameLower}";
        if(count($args) != 0)
        {
            $sql .= ' WHERE';
            foreach (array_keys($args) as $key => $value)
            {
                $sql .= $value.' = :'.$value;
                if($key < count($args)- 1)
                {
                    $sql .=' AND ';
                }
            }
        }
        $stmt = $this->cnx->prepare($sql);

        foreach ($args as $key => $value)
        {
            $stmt->bindParam(':'.$key, $value);

        }

        $stmt->execute();
        $stmt->setFetchMode(PDO::FETCH_CLASS, $className);
        $results = $stmt->fetchAll();

        return $results;
    }
    public function __destruct()
    {
        $this->cnx = null;
    }
}
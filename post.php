<?php
require_once 'vendor/autoload.php';
require_once 'entity/Post.php';

if (!array_key_exists('id', $_GET))
{
    throw new Exception("Il manque l'id de l'article");
}

$p = new Post();
$post = $p->getOneById($_GET['id']);

dd($post);
// 4- effectuer l'affichage sur le HTML

require_once 'public/post.php';
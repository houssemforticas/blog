<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Blog accueil</title>
</head>
<body>
    <h1>Liste des articles</h1>
    <?php foreach ($posts as $post) : ?>
        <div>
            <a href="post.php?id=<?= $post->getId() ?>"><h2><?= $post->getTitle() ?></h2></a>
            <a href="post.php?id=<?= $post->getId() ?>"><img src="<?= $post->getImage() ?>" alt=""></a>
            <p><?= $post->getContent() ?>...</p>
            <a href="post.php?id=<?= $post->getId() ?>">Lire la suite</a>
        </div>
    <?php endforeach; ?>
</body>
</html>